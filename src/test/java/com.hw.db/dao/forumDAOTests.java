import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;
import static org.mockito.Mockito.*;

public class forumDAOTests {
    JdbcTemplate mockJdbc;
    ForumDAO forumDAO;
    String forumSlug = "slug";
    int userLimit = 10;
    String userSince = "nickname";

    @BeforeEach
    void setup() {
        mockJdbc = mock(JdbcTemplate.class);
        forumDAO = new ForumDAO(mockJdbc);
    }

    @Test
    void UserListTest1() {
        ForumDAO.UserList(forumSlug, userLimit, userSince, false);
        String q = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname LIMIT ?;";
        verify(mockJdbc).query(Mockito.eq(q), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void UserListTest2() {
        ForumDAO.UserList(forumSlug, null, null, null);
        String q = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname;";
        verify(mockJdbc).query(Mockito.eq(q), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void UserListTest3() {
        ForumDAO.UserList(forumSlug, userLimit, userSince, true);
        String q = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc LIMIT ?;";
        verify(mockJdbc).query(Mockito.eq(q), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void UserListTest4() {
        ForumDAO.UserList(forumSlug, userLimit, null, true);
        String q = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname desc LIMIT ?;";
        verify(mockJdbc).query(Mockito.eq(q), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void UserListTest5() {
        ForumDAO.UserList(forumSlug, userLimit, null, false);
        String q = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY nickname LIMIT ?;";
        verify(mockJdbc).query(Mockito.eq(q), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void UserListTest6() {
        ForumDAO.UserList(forumSlug, null, userSince, true);
        String q = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname < (?)::citext ORDER BY nickname desc;";
        verify(mockJdbc).query(Mockito.eq(q), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }

    @Test
    void UserListTest7() {
        ForumDAO.UserList(forumSlug, null, userSince, false);
        String q = "SELECT nickname,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  nickname > (?)::citext ORDER BY nickname;";
        verify(mockJdbc).query(Mockito.eq(q), Mockito.any(Object[].class), Mockito.any(UserDAO.UserMapper.class));
    }
}