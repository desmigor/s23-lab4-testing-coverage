import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import static org.mockito.Mockito.mock;

import java.sql.Timestamp;

public class postDAOTests {

    JdbcTemplate mockedJdbc;
    PostDAO mockedPost;
    Post originalPost;
    int examplePostId = 42;
    String query1 = "SELECT * FROM \"posts\" WHERE id=? LIMIT 1;";

    @BeforeEach
    void initializeSetPost() {
        mockedJdbc = mock(JdbcTemplate.class);
        mockedPost = new PostDAO(mockedJdbc);
        originalPost = new Post("author", new Timestamp(3202999), "forum", "message", 0, 0, false);
        Mockito.when(mockedJdbc.queryForObject(Mockito.eq(query1), Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(examplePostId))).thenReturn(originalPost);
    }

    @Test
    void testSetPost1() {
        PostDAO.setPost(examplePostId, originalPost);
        Mockito.verify(mockedJdbc).queryForObject(Mockito.eq(query1), Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(examplePostId));
    }

    @Test
    void testSetPost2() {
        Post updatedPost = new Post("author2", new Timestamp(4202999), "forum2", "message2", 0, 0, false);
        PostDAO.setPost(examplePostId, updatedPost);
        String query = "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
        Mockito.verify(mockedJdbc).update(Mockito.eq(query), Mockito.anyString(), Mockito.anyString(),
                Mockito.any(Timestamp.class), Mockito.anyInt());
    }

    @Test
    void testSetPost3() {
        Post updatedPost = new Post("author2", new Timestamp(3202999), "forum", "message", 0, 0, false);
        PostDAO.setPost(examplePostId, updatedPost);
        String query = "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;";
        Mockito.verify(mockedJdbc).update(Mockito.eq(query), Mockito.anyString(), Mockito.anyInt());
    }

    @Test
    void testSetPost4() {
        Post updatedPost = new Post("author", new Timestamp(4202999), "forum", "message", 0, 0, false);
        PostDAO.setPost(examplePostId, updatedPost);
        String query = "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
        Mockito.verify(mockedJdbc).update(Mockito.eq(query), Mockito.any(Timestamp.class), Mockito.anyInt());
    }

    @Test
    void testSetPost5() {
        Post updatedPost = new Post("author", new Timestamp(3202999), "forum", "message2", 0, 0, false);
        PostDAO.setPost(examplePostId, updatedPost);
        String query = "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;";
        Mockito.verify(mockedJdbc).update(Mockito.eq(query), Mockito.anyString(), Mockito.anyInt());
    }

    @Test
    void SetPostTest6() {
        Post newPost = new Post("author2", new Timestamp(4101999), "forum", "message", 0, 0, false);
        PostDAO.setPost(examplePostId, newPost);
        String q = "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
        Mockito.verify(mockedJdbc).update(Mockito.eq(q), Mockito.anyString(), Mockito.any(Timestamp.class),
                Mockito.anyInt());
    }

    @Test
    void SetPostTest7() {
        Post newPost = new Post("author", new Timestamp(4101999), "forum", "message2", 0, 0, false);
        PostDAO.setPost(examplePostId, newPost);
        String q = "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;";
        Mockito.verify(mockedJdbc).update(Mockito.eq(q), Mockito.anyString(), Mockito.any(Timestamp.class),
                Mockito.anyInt());
    }
}
